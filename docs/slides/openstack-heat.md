---
marp: true
paginate: true
theme: default
---
<!-- _backgroundColor: #3eaff3 -->
<!-- _color: white -->
![bg left:40% 90%](assets/openstack.png)

# Openstack

## Présentation de heat

-emmanuel.braux@imt-atlantique.fr-

---
<!-- _backgroundColor: #3eaff3 -->
<!-- _color: white -->

![bg left:30% 80%](assets/openstack.png)

# Présentation de heat

##  Les grands Principes de Heat
##  Les clients heat
##  Usages avancés
---

# Licence informations

Auteur : emmanuel.braux@imt-atlantique.fr

Cette présentation est sous License Créative Commons 3.0 France (CC BY-NC-SA 3.0 FR)
Selon les options : Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions.

![cc-by-nc-sa](img/cc-by-nc-sa.png)

---
<!-- _backgroundColor: #3eaff3 -->
<!-- _color: white -->

![bg left:30% 80%](assets/openstack.png)

# Présentation de heat

##  Les grands Principes de Heat

---

# Objectifs de l'Orchestration

- Faciliter le déploiement des stacks
- Faciliter la personalisation des instances, et autres ressources
- Reproduire, distribuer facilement une architecture de référence
- Gérer le cycle de vie des resssources : création, suppression, dimensionnement (scale out)
- Décrire les infrastructures mises en place
---

# Heat

Heat est le module d'orchestration d'Openstack

- Description d'Architectures Cloud complexes dans un fichier texte (template)
- Déploiement de ces architectures (stacks)
- Personnalisation fine des instances

Rem: Heat communique directement avec les autres composants d'openstack

---

# Ecriture de template en yaml #

- **Fichier texte** compréhensible (avec un peu d'expérience)
- Manipulation de tout type d'objet et de **ressources**
- Gestion de **dépendances** entre les ressources

---

# Gestion des stack #

- Création
- Modification
- Suppression (simple, même si plusieurs dizaines de ressources)
- Portabilité, Reproductibilité

---

# Formats de template supportés #

- Heat Orchestration Template (HOT)
	- YAML

- Compatible AWS Cloud Formations Query API (CFN)
	- JSON
	- YAML

---

# Structure d'une template HOT

- définition de **`resources`** : compute, network, ...
- possibilité de prévoir des **`parameters`** :
    - personnaliser la stack,
    - noms d'instances, des mots de passe, ...
- possibilité de définir des **`outputs`** : 
    - informations à propos des ressources déployées
    - adresse IP flottante, ...

---

# Exemple de fichier HOT - part 1

## `heat_template_version`, `description` et `parameters` ##

```yaml
heat_template_version: 2014-10-16

description: Heat test template with cirros server

parameters:
  key:
    type: string
    label: Key name
    description: key-pair to be used
```

---

# Exemple de fichier HOT  - part 2 

## `resources` et `outputs` ##

```yaml
resources:
  my_instance:
    type: OS::Nova::Server
    properties:
      image: cirros-current
      flavor: m1.small
      key_name: { get_param: key }
      networks:
        - network: private-net

outputs:
  instance_ip:
    description: IP address of the instance
    value: { get_attr: [my_instance, first_address] }
```

---
<!-- _backgroundColor: #3eaff3 -->
<!-- _color: white -->

![bg left:30% 80%](assets/openstack.png)

## Présentation de heat

##  Les clients heat

---
# Le Dashboard Horizon #

- Permet de saisir une template
- Permet d'utiliser un fichier template local ou distant
- Est convivial pour la saisie des paramètres
- Est Limitée à un seul fichier (problème avec les nested template)

Comme d'habitude avec Horizon :

- Simple et convivial pour les opérations de base
- Limité lorsque qu'on souhaite utiliser les fonctions avancées

---

# En ligne de commande #

- Necessaire d'installer le client heat en plus du client openstack
- La plupart des commandes heat sont intégrées au client openstack.
- Les commandes intégrées sont sont marquées "deprecated" avec le client heat
- Certaines commandes ne sont disponibles que via le client heat

---

# Installation des clients Openstack et Heat #

- Des packages existent pour les différentes distributions
- Avec "python/pip" (environnement "virtualenv" recommandé)

## Installation avec les package Ubuntu ##

```console
apt install -y python-openstackclient
apt install -y python-heatclient
```

## Installation via python PIP ##

```console
pip install python-openstackclient
pip install python-heatclient
```

---

# Configuration de l'authentification #

- Utilisation des variables en ligne de commande
- Utilisation d'un fichier RC
- Utilisation d'un fichier cloud.yaml

> (Le client heat ne supporte pas l'authentification via fichier cloud.yaml)

---

# Principales commandes #

## Actions de type CRUD ##

```console
openstack stack create  -t fichier_HOT.yaml heat_01
openstack stack update heat_01

openstack stack list
openstack stack show heat_01

openstack stack delete heat_01
```

## Informations complémentaires ##

```console
openstack stack event list heat_01

openstack stack heat_01 output show --all
```

---
<!-- _backgroundColor: #3eaff3 -->
<!-- _color: white -->

![bg left:30% 80%](assets/openstack.png)

## Présentation de heat

## Usages avancés

---


- **Automatisation** des déploiement
- Instances **High Avability**  / **Auto-Scaling**
    - lancement de **plusieurs** éléments identiques
    -  lancement / arrêt de ressources en **fonction de la charge** (couplé avec Telemetry)
- Templates **imbriquées (nested**) : meilleure gestion des stacks complexes
