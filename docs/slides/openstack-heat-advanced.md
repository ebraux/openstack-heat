---
marp: true
paginate: true
theme: default
---
<!-- #3eaff3 bleu ciel -->
<!-- #0074d0 bleu electrique -->
<!-- _backgroundColor: #3eaff3 -->
<!-- _color: white -->
![bg left:40% 90%](assets/openstack.png)

# Openstack

## Orchestration avec heat

-emmanuel.braux@imt-atlantique.fr-

---
![bg left:30% 80%](assets/openstack.png)

# Objectifs

**Connaitre les possibilités de HEAT**
**Acquérir les notions de base pour comprendre et créer des templates**
**Etre en mesure d'installer et d'utiliser heat**

---
<!-- _backgroundColor: #3eaff3 -->
<!-- _color: white -->
![bg left:30% 80%](assets/openstack.png)

# Orchestration avec heat

**Introduction**
**Principes de Heat**
**Heat Orchestration Template (HOT)**
**Les clients**
**Utilisation avancée**

---

# Licence informations


Auteur : emmanuel.braux@imt-atlantique.fr

Cette présentation est sous License Créative Commons 3.0 France (CC BY-NC-SA 3.0 FR)
Selon les options : Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions.

![cc-by-nc-sa](img/cc-by-nc-sa.png)

---
<!-- _backgroundColor: #3eaff3 -->
<!-- _color: white -->
![bg left:30% 80%](assets/openstack.png)

# Orchestration avec heat

## Introduction

---

# Rappel des fonctionnalités d'Openstack #

- Différents types de ressources (server, network, volumes, ...)
- Accès à la demande à ces ressources
- Des mécanismes permettant d'assembler ces ressources, pour créer une infrastructure
- Possibilité de personnaliser les instances lancées (user-data, cloud-init)

---

# Les outils disponibles ##

- **Le tableau de bord Horizon :** visuel, simple, plus ou moins rapide, répétitif
- **Les outils en ligne de commande :** rapides, peuvent être scriptés avec un minimum de compétences
- **Les librairies (python, java...) :** plus complètes, mais :
    * mise en oeuvre plus longue
    * plus de compétences spécifiques.
- **Outils tierces :** terraform, ansible, ...

---

# Problématique des architectures Cloud 

- Des architectures de plus en plus complexes
- Un grand nombre de ressources, de liens
- Des configurations d'instances automatisables
- La promesse d'environnements à la demande (dev /recette / prod)
- Des besoins interropérabilité entre clouds

---

# Objectifs de l'Orchestration

- Faciliter le déploiement des stacks
- Faciliter la personalisation des instances, et autres ressources
- Reproduire, distribuer facilement une architecture de référence
- Gérer le cycle de vie des resssources : création, suppression, dimensionnement (scale out)
- Décrire les infrastructures mises en place

---

# Heat

Heat est le module d'orchestration d'Openstack

- Description d'Architectures Cloud complexes dans un fichier texte (template)
- Déploiement de ces architectures (stacks)
- Personnalisation fine des instances

Rem: Heat communique directement avec les autres composants d'openstack

---

# Historique

Intégration officielle dans le projet Openstack :  Havana (fin 2013)

Inspiré de `CloudFormation` (CFN) proposé par AWS

---
<!-- _backgroundColor: #3eaff3 -->
<!-- _color: white -->
![bg left:30% 80%](assets/openstack.png)

# Orchestration avec heat

## Principes de Heat

---

# Ecriture de template en yaml #

- **Fichier texte** compréhensible (avec un peu d'expérience)
- Manipulation de tout type d'objet et de **ressources**
- Gestion de **dépendances** entre les ressources

---

# Gestion des stack #

- Création
- Modification
- Suppression (simple, même si plusieurs dizaines de ressources)
- Portabilité, Reproductibilité

---

# Usages avancés #

- **Automatisation** des déploiement
- Instances **High Avability**  / **Auto-Scaling**
    - lancement de **plusieurs** éléments identiques
    -  lancement / arrêt de ressources en **fonction de la charge** (couplé avec Telemetry)
- templates **imbriquées (nested**) : meilleure gestion des stacks complexes

---

# Formats de template supportés #

-* Heat Orchestration Template (HOT)
	- YAML

- Compatible AWS Cloud Formations Query API (CFN)
	- JSON
	- YAML

---
<!-- _backgroundColor: #3eaff3 -->
<!-- _color: white -->
![bg left:30% 80%](assets/openstack.png)

# Orchestration avec heat

## Heat Orchestration Template (HOT)

---

# Structure d'une template HOT #

- définition de **`resources`** : compute, network, ...
- possibilité de prévoir des **`parameters`** :
    - personnaliser la stack,
    - noms d'instances, des mots de passe, ...
- possibilité de définir des **`outputs`** : 
    - informations à propos des ressources déployées
    - adresse IP flottante, ...

---

# Exemple de `resources` #

## `resources` : une instance ##

```yaml
  cirros_server:
    type: OS::Nova::Server
    properties:
      name: "myCirros"
      image: "cirros"
      flavor: "m1.tiny"
      networks:
        - network : "myNetwork"
```

---

# Types de `resources` #

## Types de resources les plus courants ##

```yaml
OS::Nova::Server
OS::Neutron::Net
OS::Neutron::Port
OS::Neutron::FloatingIP
OS::Neutron::SecurityGroup
OS::Cinder::Volume
```

---

# Exemple de `parameters` #

## `parameters` : le nom d'une instance ##

```yaml
  instance_name:
    type: string
    description: "Nom de l'instance"
    default: "myInstance"
```

---

# Exemple d' `outputs` #

## `outputs` : adresse IP affectée à une instance ##

```yaml
  instance_private_ip:
    description: "Adresse IP privee du serveur"
    value: { get_attr: [ my_server, first_address }
```

---

# Autres éléments : Version de fichier template #

- champ **"heat_template_version"**
- Attribut **Obligatoire**
- Compatibilité des templates avec :
    - Les composants d'openstack
    - Les fonctionnalités disponibles dans Heat
- Valeur
    - La date de "release" de Heat
    - Depuis newton : peut également être la version de Heat
    - Version minimum de la template `2013-05-23` (Icehouse )

---

# Exemple de valeur pour "heat_template_version" #

## `heat_template_version` exemples ##

```yaml
heat_template_version: 2013-05-23
```

```yaml
heat_template_version: 2014-10-16
```

```yaml
heat_template_version: ocata
```

---

# Autres éléments : Description #

- Champ **"description"**
- Optionnel, mais fortement recommandé
- Caracteres : alphanumériques (pas de ":")
- Peut être sur une ou plusieurs lignes :

## Exemples de `description` ##

```yaml
description: Heat cirros server template
```

```yaml
description: >
  Heat WordPress template.
  This template installs a single-instance.
```

---

# Exemple de fichier HOT Basique - part 1 #

## `heat_template_version`, `description` et `parameters` ##

```yaml
heat_template_version: 2014-10-16

description: Heat test template with cirros server

parameters:
  key:
    type: string
    label: Key name
    description: key-pair to be used
```

---

# Exemple de fichier HOT Basique - part 2 #

## `resources` et `outputs` ##

```yaml
resources:
  my_instance:
    type: OS::Nova::Server
    properties:
      image: cirros-current
      flavor: m1.small
      key_name: { get_param: key }
      networks:
        - network: private-net

outputs:
  instance_ip:
    description: IP address of the instance
    value: { get_attr: [my_instance, first_address] }
```

---
<!-- _backgroundColor: #3eaff3 -->
<!-- _color: white -->
![bg left:30% 80%](assets/openstack.png)

# Orchestration avec heat

## Les clients

---

# Le Dashboard Horizon #

- Permet de saisir une template
- Permet d'utiliser un fichier template local ou distant
- Est convivial pour la saisie des paramètres
- Est Limitée à un seul fichier (problème avec les nested template)

Comme d'habitude avec Horizon :

- Simple et convivial pour les opérations de base
- Limité lorsque qu'on souhaite utiliser les fonctions avancées

---

# En ligne de commande #

- Necessaire d'installer le client heat en plus du client openstack
- La plupart des commandes heat sont intégrées au client openstack.
- Les commandes intégrées sont sont marquées "deprecated" avec le client heat
- Certaines commandes ne sont disponibles que via le client heat

---

# Installation des clients Openstack et Heat #

- Des packages existent pour les différentes distributions
- Avec "python/pip" (environnement "virtualenv" recommandé)

## Installation avec les package Ubuntu ##

```console
apt install -y python-openstackclient
apt install -y python-heatclient
```

## Installation via python PIP ##

```console
pip install python-openstackclient
pip install python-heatclient
```

---

# Configuration de l'authentification #

- Utilisation des variables en ligne de commande
- Utilisation d'un fichier RC
- Utilisation d'un fichier cloud.yaml

> (Le client heat ne supporte pas l'authentification via fichier cloud.yaml)

---

# Principales commandes #

## Actions de type CRUD ##

```console
openstack stack create  -t fichier_HOT.yaml heat_01
openstack stack update heat_01

openstack stack list
openstack stack show heat_01

openstack stack delete heat_01
```

## Informations complémentaires ##

```console
openstack stack event list heat_01

openstack stack heat_01 output show --all
```

---
<!-- _backgroundColor: #3eaff3 -->
<!-- _color: white -->
![bg left:30% 80%](assets/openstack.png)

# Orchestration avec heat

## Utilisation avancée

---

# Gestion de contraintes sur des paramètres #

La propriété `constraints` permet de limiter / sécuriser la saise de paramètres.

## Exemple : choix du gabarit ##

```yaml
parameter:
  flavor_name:
    default: m1.tiny
    ...
    constraints:
      - allowed_values:
        - m1.tiny
        - m1.small
```

---

# Autres types de contraintes #

## Format ##
```yaml
  - range: { min: 0, max: 10 }
  - length: { min: 6, max: 8 }
  - allowed_pattern: "^[a-zA-Z0-9.-_]{1,255}$"
  - ...
```

## Lien avec des ressources existantes ##

```yaml
  - custom_constraint: nova.keypair
    description: Must name keypair known to Nova

  - custom_constraint: nova.flavor
    description: Must be a valid flavor
```

---

# Regroupement de paramètres : parameter_groups #

Permet de regouper des paramètres.

## Exemple de `parameter_groups` ##

```yaml
parameter_groups:
 - label: General parameters
   parameters:
     - keypair_name
     - flavor_name
```

---

# Personnalisation des instances : user_data #

## Exemple d'utilisation : lancement d'une commande shell ##

```yaml
resources:
  my_instance:
    type: OS::Nova::Server
    properties:
      ...
      user_data_format: RAW
      user_data: |
        #!/bin/sh
        echo "Hello, World!"
```

---

# Personnalisation des instances : user_data #

## Exemple d'utilisation d'un shell bash avec un paramètre ##

```yaml
  user_data_format: RAW
  user_data:
    str_replace:
      params:
        __PASSWORD__: { get_param: password }
      template: |
        #!/bin/bash
        echo "ubuntu:__PASSWORD__" | chpasswd
```

---

# Personnalisation des instances : user_data #

## Exemple d'utilisation de cloudinit ##

```yaml
 user_data_format: SOFTWARE_CONFIG
 user_data: |
   #cloud-config
   password: mysecret
   chpasswd: { expire: False }
   ssh_pwauth: True

   # Install the LAMP stack
   packages:
     - apache2
     - libapache2-mod-php
```

---

# Nested templates #

Mécanisme de template parent/enfant.

## Nested template network et instance ##
```yaml
resources:
  lab_network:
    type: lib/private_network.yaml

  lab_instance:
    type: lib/default_instance.yaml
    properties:
      key: { get_param: key }
      name: { get_param: name }
      private_network: { get_attr: [lab_network, name] }
      secgroup_name: lab_sec_group
```

> Pour pouvoir utiliser 'get_attr', il faut que la nested template renvoie l'attribut dans sa section 'outputs'

---

# Et beaucoup d'autres #

- Déploiement avancé: `OS::Heat::SoftwareConfig`
- Déploiement en masse: `OS::Heat::ResourceGroup`
- Auto-sacling : `OS::Heat::Autoscaling`
- Ressources personnalisées : `OS::Nova::Server::MonServer`
- ...

---

# Conclusion #

- Heat est un outil puissant
- La prise en main est complexe mais reste accessible
- Il vaut mieux construire ses templates de façon progressive
- Nécessaire de structurer ses templates
- Refléchir à l'intégration avec les outils d'orchestration pré-existants

---

# Références : Documentation Officielle Openstack #

- Heat
    - <https://www.openstack.org/software/releases/queens/components/heat>
    - <https://docs.openstack.org/heat/latest/index.html>
    - <https://wiki.openstack.org/wiki/Heat>
    - <https://docs.openstack.org/heat/latest/getting_started/create_a_stack.html>
  
- Les clients :
    - Utilisation d'Horizon :
       - <https://docs.openstack.org/horizon/pike/user/stacks.html>
    - Utilisation du client en ligne de commande
        - <https://docs.openstack.org/ocata/cli-reference/heat.htm>

---

# Références : Templates HOT #

* Références:
  * <https://docs.openstack.org/heat/latest/template_guide/>
  * <https://docs.openstack.org/heat/latest/template_guide/hot_guide.html>
  * <https://docs.openstack.org/heat/latest/template_guide/hello_world.html>
* Les versions :
  * <https://docs.openstack.org/heat/latest/template_guide/hot_spec.html#hot-spec-template-version>
* Les contraintes sur les parametres
  * <https://docs.openstack.org/heat/latest/template_guide/hot_spec.html#hot-spec-parameters-constraints>

---

# Références : Autres Ressources #

- Présentations / didactitiels
    - <https://www.youtube.com/watch?v=G_eUp42t-ZI>
    - <https://2014.rmll.info/slides/237/openstack-heat-rmll2014.pdf>
- Exemples de template :
    - <https://github.com/openstack/heat-templates>
    - <https://github.com/rackspace-orchestration-templates>
    - <https://github.com/cloudwatt/applications>

---

# Références : Divers Sujets Abordés #

- Amazon AWS Cloud Formation :
    - <https://docs.aws.amazon.com/AWSCloudFormation/latest/APIReference/Welcome.html>
- Python Virtualenv :
    - <https://virtualenv.pypa.io/en/stable/>
- Cloudinit :
    - <http://cloudinit.readthedocs.io/en/latest/>

